const repl = require('repl');
const mongoose = require('mongoose');
const fileSystem = require('fs');
const path = require('path');

/**
 * The Mongoose Database configuration file
 * must be placed under a config/ folder
 * on a database.config.js or db.config.js file
 */
let dbConfig;
const configDirectory = path.join(__dirname, 'config');
if(fileSystem.existsSync(`${configDirectory}/database.config.js`)) {
    dbConfig = require('./config/database.config');
} else if(fileSystem.existsSync(`${configDirectory}/db.config.js`)) {
    dbConfig = require('./config/db.config');
} else {
    console.log('Neither config/database.config.js nor config/db.config.js was found.');
}


async function startConsole() {
    /**
     * This code will automatically load all models placed in app/models folder
     */
    let models = new Array();
    const modelsDirectory = path.join(__dirname, 'app/models');
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }
    await fileSystem.readdir(modelsDirectory, (err, files) => {
        if(err) {
            return console.error(`Unable to read ${modelsDirectory} folder.`);
        }
        files.forEach((file, index) => {
            models.push({
                model: file.replace(/[$\.].*/, '').split(/_/g).map(partOfName => capitalize(partOfName)).join(''),
                path: `${modelsDirectory}/${file}`
            })
        })
    })

    /**
     * The connection to the database is created
     * and the REPL server is started
     */
    await mongoose.connect(dbConfig.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }).then(_ => {
        console.log('Connected to database');
        console.log('quit(), exit() or Ctrl+C to quit the application');
        console.log('.editor to enter Editor mode. Useful to treat the console more like an IDE')
        console.log()
    
        var replServer = repl.start({
            prompt: 'MongooseConsole > ',
        })
    
        models.forEach(model => {
            replServer.context[`${model.model}`] = require(model.path)
        })

        function endConsole() {
            console.log('Bye-bye!')
            process.exit()
        }

        replServer.context.exit = endConsole
        replServer.context.quit = endConsole
    }).catch((err) => {
        console.log('\n')
        console.error(`${err}`)
        process.exit(1)
    });
}

startConsole()